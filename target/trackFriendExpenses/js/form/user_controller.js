'use strict';
 
App.controller('UserController', ['$scope', 'UserService', function($scope, UserService) {
          var self = this;
          self.user={firstName:null ,expenseType:'',expenseAmount:'' };
          self.users=[];
          $scope.users= self.users;
          $scope.expenseType=[];

          $scope.getExpenseTypes = function(){
          
              UserService.getExpenseType()
                  .then(
                               function(d) {
                                    $scope.expenseType = d;
                               },
                                function(errResponse){
                                    console.error('Error while fetching Currencies');
                                }
                       );
            };
          
	    
               
          self.fetchAllUsers = function(){
              UserService.fetchAllUsers()
                  .then(
                               function(d) {
                               var changedStruct=[];
                               self.users = d;
                               },
                                function(errResponse){
                                    console.error('Error while fetching Currencies');
                                }
                       );
          };




          self.createUser = function(user ){
              UserService.createUser(user)
                      .then(
                      self.fetchAllUsers, 
                              function(errResponse){
                                   console.error('Error while creating User.');
                              } 
                  );
          };
 
         self.updateUser = function(user){
              UserService.updateUser(user, "Zirgs")
                      .then(
                              self.fetchAllUsers, 
                              function(errResponse){
                                   console.error('Error while updating User.');
                              } 
                  );
          };
 
         self.deleteUser = function(user){
              UserService.deleteUser(user)
                      .then(
                              self.fetchAllUsers, 
                              function(errResponse){
                                   console.error('Error while deleting User.');
                              } 
                  );
          };
 
       
          self.submit = function() {
              if(self.user.firstName!=null){
                  console.log('Saving New User', self.user);    
                  self.createUser(self.user);
                   //self.fetchAllUsers();
                              
              }else{
                  self.updateUser(self.user, self.user.firstName);
                  console.log('User updated with id ', self.user.firstName);
              }
              self.reset();
          };
               
          self.edit = function(firstName){
              console.log('firstName to be edited', firstName);
              for(var i = 0; i < self.users.length; i++){
                  if(self.users[i].firstName === firstName) {
                     self.user = angular.copy(self.users[i]);
                     break;
                  }
              }
          };
               
          self.remove = function(firstName){
              console.log('firstName to be deleted', firstName);
              if(self.user.firstName === firstName) {//clean form if the user to be deleted is shown there.
                 self.reset();
              }
              self.deleteUser(firstName);
          };
 
           
          self.reset = function(){
              self.user={firstName:'',expenseType:'',expenseAmount:''};
              $scope.myForm.$setPristine(); //reset Form
          };
 
      }]);