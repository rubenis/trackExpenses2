<html>
  <head>  
    <title>AngularJS $http Example</title>  
    <style>
      .username.ng-valid {
          background-color: lightgreen;
      }
      .username.ng-dirty.ng-invalid-required {
          background-color: red;
      }
      .username.ng-dirty.ng-invalid-minlength {
          background-color: yellow;
      }
 
      .email.ng-valid {
          background-color: lightgreen;
      }
      .email.ng-dirty.ng-invalid-required {
          background-color: red;
      }
      .email.ng-dirty.ng-invalid-email {
          background-color: yellow;
      }
 
    </style>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    
  </head>
  <body ng-app="myApp" class="ng-cloak">
      <div class="generic-container" ng-controller="UserController as ctrl" ng-init="getExpenseTypes()">
          <div class="panel panel-default">
              <div class="panel-heading"><span class="lead">Track Friend Expenses</span></div>
              <div class="formcontainer">
                  <form ng-submit="ctrl.submit()" name="myForm" class="form-horizontal">
                      <input type="hidden" ng-model="ctrl.user.firstName" />
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="firstName">Friend Name</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.user.firstName" id="firstName" class="username form-control input-sm" placeholder="Enter your name" required ng-minlength="3"/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.firstName.$error.required">This is a required field</span>
                                      <span ng-show="myForm.firstName.$error.minlength">Minimum length required is 3</span>
                                      <span ng-show="myForm.firstName.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>
                         
                       
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="expenseType">expenseType</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.user.expenseType" id="expense" class="form-control input-sm" placeholder="Enter Expense type. [This field is validation free]"/>
                              </div>
                              <select ng-model="ctrl.user.expenseType" ng-options="x for x in expenseType">
</select>
                          </div>
                      </div>
 
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="amount">Expense Amount</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.user.expenseAmount" id="amount" class="email form-control input-sm" placeholder="Spend money value" required/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.amount.$error.required">This is a required field</span>
                                      <span ng-show="myForm.amount.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>
 
                      <div class="row">
                          <div class="form-actions floatRight">
                              <input type="submit"  value="{{(ctrl.user.firstName) ? 'Add' : 'Update'}}" class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid">
                              <button type="button" ng-click="ctrl.reset()" class="btn btn-warning btn-sm" ng-disabled="myForm.$pristine">Reset Form</button>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
          <div class="panel panel-default">
                <!-- Default panel contents -->
              <div class="panel-heading"><span class="lead">List of Users </span></div>
              <div class="tablecontainer">
                  <table class="table table-hover">
                      <thead>
                          <tr>
                              <th>Friend Name</th>
                              <th>Expense type</th>
                              <th>Expense amount</th>
                              <th width="20%"></th>
                          </tr>
                      </thead>
                      <tbody>
                          <tr ng-repeat="u in ctrl.users">
                              <td><span>{{u.firstName}}</span></td>
                              <td><span ng-repeat="(expenseName, expenseAmount) in u.expenseType">{{expenseName}}</span></td>
                              <td><span ng-repeat="(expenseName, expenseAmount) in u.expenseType">{{expenseAmount}}</span></td>
                              <td>
                              <button type="button" ng-click="ctrl.edit(u.firstName)" class="btn btn-success custom-width">Edit</button>  <button type="button" ng-click="ctrl.remove(u.firstName)" class="btn btn-danger custom-width">Remove</button>
                              </td>
                          </tr>
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
       
      <script src="lib/angular/angular.js"></script>
      <script src="js/form/app.js"></script>
      <script src="js/form/user_service.js"></script>
      <script src="js/form/user_controller.js"></script>
  </body>
</html>