package ngdemo.rest;

import ngdemo.Friends.SerializedFriend;
import ngdemo.Friends.ExpenseDefinition;
import ngdemo.Friends.FriendStructure;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.POST;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.google.gson.Gson;

@Path("/users")
public class restService {

    static Logger log = Logger.getLogger(restService.class);

    HashMap<String, FriendStructure> globalUserList = new HashMap<String, FriendStructure>();

    @Context
    private ServletContext context;

    @POST
    @Path("/addUser")
    @Consumes({ "application/json" })
    @Produces(MediaType.APPLICATION_JSON)
    public Response addUserExpenses(SerializedFriend user) {

        globalUserList = (HashMap<String, FriendStructure>) context.getAttribute("userList");

        //If user already exists add increase its expense <HashMap>;
        if (globalUserList.containsKey(user.getFirstName())) {
            globalUserList.get(user.getFirstName()).addExpenses(user.getExpenseType(), user.getExpenseAmount());
            log.info("User with additional expense" + new Gson().toJson(globalUserList));
        } else {

            //Else add a new friend;
            FriendStructure registeredFriend = new FriendStructure();
            registeredFriend.setFirstName(user.getFirstName());
            log.info("expenses set " + user.getExpenseType());
            log.info("expense amount" + user.getExpenseAmount());
            registeredFriend.addExpenses(user.getExpenseType(), user.getExpenseAmount());

            globalUserList.put(user.getFirstName(), registeredFriend);
            log.info("New user" + new Gson().toJson(Arrays.asList(registeredFriend)));
        }
        context.setAttribute("userList", globalUserList);
        return Response.ok().entity("{\"Submit\": \"Success\"}").build();
    }

    @GET
    @Path("/allUsers")
    @Consumes({ "application/json" })
    @Produces(MediaType.APPLICATION_JSON)
    public Response fetchAll() {
        List<FriendStructure> updatedResponseList = new ArrayList<FriendStructure>();
        HashMap<String, FriendStructure> globalUserList = (HashMap<String, FriendStructure>) context.getAttribute("userList");
        log.info("Fetch ALL" + new Gson().toJson(globalUserList));

        //Iterate find diferent expenses
        Iterator keys = globalUserList.entrySet().iterator();

        while (keys.hasNext()) {
            Map.Entry pair = (Map.Entry) keys.next();
            FriendStructure processedFriend = (FriendStructure) pair.getValue();

            if (processedFriend.getExpenseType().size() > 1) {
                for (ExpenseDefinition each : (Set<ExpenseDefinition>) processedFriend.getExpenseType().keySet()) {
                    log.info(each.name());
                    FriendStructure updatedFriend = new FriendStructure();
                    updatedFriend.setFirstName(processedFriend.getFirstName());
                    updatedFriend.addExpenses(each.name(), processedFriend.getExpenseAmount(each.name()));
                    updatedResponseList.add(updatedFriend);
                }
            } else {
                updatedResponseList.add(processedFriend);
            }
        }

        context.setAttribute("userList", globalUserList);

        log.info("Fetch ALL JSON :" + new Gson().toJson(updatedResponseList));

        if (!updatedResponseList.isEmpty()) {
            return Response.ok().entity(new Gson().toJson(updatedResponseList)).build();
        } else {
            return Response.ok().entity(new Gson().toJson(globalUserList)).build();
        }
    }

    @GET
    @Path("/expenseType")
    @Produces(MediaType.APPLICATION_JSON)
    public Response readExpenses() {
        return Response.ok().entity(ExpenseDefinition.values()).build();

    }
}
