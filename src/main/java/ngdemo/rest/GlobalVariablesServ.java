package ngdemo.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import ngdemo.Friends.FriendStructure;

public class GlobalVariablesServ implements ServletContextListener{

    public void contextInitialized(ServletContextEvent sce) {
    	Map<String, FriendStructure> registeredUsers = new HashMap<String, FriendStructure>();
         ServletContext context = sce.getServletContext();
         context.setAttribute("userList", registeredUsers);
    }

    public void contextDestroyed(ServletContextEvent sce) {
        ServletContext context = sce.getServletContext();
        context.removeAttribute("userList");
    }

}
