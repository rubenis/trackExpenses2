package ngdemo.Friends;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class SerializedFriend {

	private String firstName;
	private String expenseType;
    private Integer expenseAmount;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setExpenseType(String expenses) {
		this.expenseType = expenses;
	}

	public String getExpenseType() {
		return this.expenseType;
	}

	public Integer getExpenseAmount() {
		return this.expenseAmount;
	}

}
