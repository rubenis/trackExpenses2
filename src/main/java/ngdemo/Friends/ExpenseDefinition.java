package ngdemo.Friends;

public enum ExpenseDefinition {
 RoadExpense(0),
   Museum(1),
   Cinema(2);

	
   private int value;
   private ExpenseDefinition(int value) {
      this.value = value;
   }
   public int getValue() {
      return value;
   }
}

