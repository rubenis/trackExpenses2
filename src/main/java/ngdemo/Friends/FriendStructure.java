package ngdemo.Friends;

import ngdemo.Friends.ExpenseDefinition;
import java.util.HashMap;

public class FriendStructure {

    private String firstName;

    private HashMap<ExpenseDefinition, Integer> expenseType = new HashMap<ExpenseDefinition, Integer>();

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void addExpenses(String expenses, Integer value) {

        this.expenseType.put(ExpenseDefinition.valueOf(expenses), value);
        //expenseAmount = value;
    }

    public Integer getExpenseAmount(String expenses) {
        return this.expenseType.get(ExpenseDefinition.valueOf(expenses));
}

    public String getFirstName() {
        return firstName;
    }

    public HashMap getExpenseType() {
        return this.expenseType;
    }
}
